from app.main import Queen, King, Pawn, Knight, Bishop, Rook


class TestClass:
    def test_constructor(self):
        self.king = King("a1")
        self.queen = Queen("a3")
        self.pawn = Pawn("h4")
        self.knight = Knight("h7")
        self.bishop = Bishop("f5")
        self.rook = Rook("d4")

        assert self.king.position == "a1"
        assert self.queen.position == "a3"
        assert self.pawn.position == "h4"
        assert self.knight.position == "h7"
        assert self.bishop.position == "f5"
        assert self.rook.position == "d4"

    def test_list_available_moves(self):
        self.king = King("a1")
        self.queen = Queen("a3")
        self.pawn = Pawn("h4")
        self.knight = Knight("h7")
        self.bishop = Bishop("d2")
        self.rook = Rook("d4")

        av_moves = ["a2", "b2", "b1"]
        assert self.king.list_available_moves() == av_moves
        av_moves = [
            "b3",
            "c3",
            "d3",
            "e3",
            "f3",
            "g3",
            "h3",
            "a8",
            "a7",
            "a6",
            "a5",
            "a4",
            "a2",
            "a1",
            "f8",
            "e7",
            "d6",
            "c5",
            "b4",
            "c1",
            "b2",
        ]
        assert self.queen.list_available_moves() == av_moves
        av_moves = ["h5"]
        assert self.pawn.list_available_moves() == av_moves
        av_moves = ["f8", "f6", "g5"]
        assert self.knight.list_available_moves() == av_moves
        av_moves = ["h6", "g5", "f4", "e3", "c1", "a5", "b4", "c3", "e1"]
        assert self.bishop.list_available_moves() == av_moves
        av_moves = [
            "a4",
            "b4",
            "c4",
            "e4",
            "f4",
            "g4",
            "h4",
            "d8",
            "d7",
            "d6",
            "d5",
            "d3",
            "d2",
            "d1",
        ]
        assert self.rook.list_available_moves() == av_moves

    def test_validate_move(self):

        self.king = King("a1")
        self.queen = Queen("a3")
        self.pawn = Pawn("h4")
        self.knight = Knight("h7")
        self.bishop = Bishop("d2")
        self.rook = Rook("d4")

        assert self.king.validate_move("a2")
        assert False if self.king.validate_move("a8") else True

        assert self.queen.validate_move("a2")
        assert False if self.queen.validate_move("b1") else True

        assert self.pawn.validate_move("h5")
        assert False if self.pawn.validate_move("h7") else True

        assert self.knight.validate_move("f6")
        assert False if self.knight.validate_move("c2") else True

        assert self.bishop.validate_move("c1")
        assert False if self.bishop.validate_move("c8") else True

        assert self.rook.validate_move("g4")
        assert False if self.rook.validate_move("c8") else True
