# Chess
Aby uruchomić serwer należy:
1. stworzyć nowe wirtualne środowisko :
`virtualenv env`
2. zainstalować moduły 
`pip install -r requirements.txt`
3. uruchomić serwer:
`python app/main.py`

endpointy:
"http://0.0.0.0:5000/api/v1/{nazwa figury}/{pole figury}"
"http://0.0.0.0:5000/api/v1/{nazwa figury}/{pole figury}/{pole na które mamy się ruszyć}"

testy:
`pytest`

Kod został sformatowany za pomocą Black i Flake8


//Docker

Aplikacja umożliwia konteneryzację.
Aby uruchomić serwer w Dockerze należy:
1.Posiadać zainstalowany Docker i docker compose
2.Utworzyć kontenery:
`sudo docker-compose build`
3.Postawić kontenery:
`sudo docker-compose up`