import requests

BASE = "http://0.0.0.0:5000/api/v1"


class TestRoutes:
    def test_rook(self):

        r = requests.get(BASE + "/rook/h8")
        av_moves = [
            "h1",
            "h2",
            "h3",
            "h4",
            "h5",
            "h6",
            "h7",
            "a8",
            "b8",
            "c8",
            "d8",
            "e8",
            "f8",
            "g8",
        ]
        status = 200
        fig = "rook"
        field = "h8"
        data = r.json().get("data")
        error = None

        assert status == r.status_code
        for av in av_moves:
            assert av in data.get("availableMoves")
        assert len(av_moves) == len(data.get("availableMoves"))
        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")

    def test_rook_move_validation(self):

        r = requests.get(BASE + "/rook/h8/h5")
        status = 200
        fig = "rook"
        field = "h8"
        data = r.json().get("data")
        error = None
        move = "valid"

        assert status == r.status_code
        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")
        assert move == data.get("move")

        r = requests.get(BASE + "/rook/h8/a6")
        status = 200
        fig = "rook"
        field = "h8"
        data = r.json().get("data")
        error = "Current move is not permitted."
        move = "invalid"

        assert status == r.status_code
        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")
        assert move == data.get("move")

    def test_king_get(self):
        r = requests.get(BASE + "/king/a1")
        av_moves = ["a2", "b1", "b2"]
        status = 200
        fig = "king"
        field = "a1"
        data = r.json().get("data")
        error = None

        assert status == r.status_code
        for av in av_moves:
            assert av in data.get("availableMoves")
        assert len(av_moves) == len(data.get("availableMoves"))
        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")

    def test_king_move_validation(self):

        r = requests.get(BASE + "/king/a1/a2")
        status = 200
        fig = "king"
        field = "a1"
        data = r.json().get("data")
        error = None
        move = "valid"

        assert status == r.status_code
        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")
        assert move == data.get("move")

        r = requests.get(BASE + "/king/a1/a6")
        status = 200
        fig = "king"
        field = "a1"
        data = r.json().get("data")
        error = "Current move is not permitted."
        move = "invalid"

        assert status == r.status_code
        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")
        assert move == data.get("move")

    def test_queen_get(self):
        r = requests.get(BASE + "/queen/a1")
        av_moves = [
            "a2",
            "a3",
            "a4",
            "a5",
            "a6",
            "a7",
            "a8",
            "b1",
            "c1",
            "d1",
            "e1",
            "f1",
            "g1",
            "h1",
            "b2",
            "c3",
            "d4",
            "e5",
            "f6",
            "g7",
            "h8",
        ]
        status = 200
        fig = "queen"
        field = "a1"
        data = r.json().get("data")
        error = None

        assert status == r.status_code
        for av in av_moves:
            assert av in data.get("availableMoves")
        assert len(av_moves) == len(data.get("availableMoves"))
        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")

    def test_queen_move_validation(self):

        r = requests.get(BASE + "/queen/a1/g1")
        status = 200
        fig = "queen"
        field = "a1"
        data = r.json().get("data")
        error = None
        move = "valid"

        assert status == r.status_code

        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")
        assert move == data.get("move")

        r = requests.get(BASE + "/queen/a1/c2")
        status = 200
        fig = "queen"
        field = "a1"
        data = r.json().get("data")
        error = "Current move is not permitted."
        move = "invalid"

        assert status == r.status_code

        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")
        assert move == data.get("move")

    def test_bishop(self):
        r = requests.get(BASE + "/bishop/h5")
        av_moves = ["d1", "e2", "f3", "g4", "e8", "f7", "g6"]
        status = 200
        fig = "bishop"
        field = "h5"
        data = r.json().get("data")
        error = None

        assert status == r.status_code
        for av in av_moves:
            assert av in data.get("availableMoves")
        assert len(av_moves) == len(data.get("availableMoves"))

        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")

    def test_bishop_move_validation(self):

        r = requests.get(BASE + "/bishop/h5/g4")
        status = 200
        fig = "bishop"
        field = "h5"
        data = r.json().get("data")
        error = None
        move = "valid"

        assert status == r.status_code

        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")
        assert move == data.get("move")

        r = requests.get(BASE + "/bishop/h5/c8")
        status = 200
        fig = "bishop"
        field = "h5"
        data = r.json().get("data")
        error = "Current move is not permitted."
        move = "invalid"

        assert status == r.status_code

        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")
        assert move == data.get("move")

    def test_knight(self):
        r = requests.get(BASE + "/knight/c3")
        av_moves = ["b5", "d5", "a4", "e4", "a2", "e2", "b1", "d1"]
        status = 200
        fig = "knight"
        field = "c3"
        data = r.json().get("data")
        error = None

        assert status == r.status_code
        for av in av_moves:
            assert av in data.get("availableMoves")
        assert len(av_moves) == len(data.get("availableMoves"))

        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")

    def test_knight_move_validation(self):

        r = requests.get(BASE + "/knight/c3/b1")
        status = 200
        fig = "knight"
        field = "c3"
        data = r.json().get("data")
        error = None
        move = "valid"

        assert status == r.status_code

        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")
        assert move == data.get("move")

        r = requests.get(BASE + "/knight/c3/c8")
        status = 200
        fig = "knight"
        field = "c3"
        data = r.json().get("data")
        error = "Current move is not permitted."
        move = "invalid"

        assert status == r.status_code

        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")
        assert move == data.get("move")

    def test_pawn(self):
        r = requests.get(BASE + "/pawn/h2")
        av_moves = ["h3"]
        status = 200
        fig = "pawn"
        field = "h2"
        data = r.json().get("data")
        error = None

        assert status == r.status_code
        for av in av_moves:
            assert av in data.get("availableMoves")
        assert len(av_moves) == len(data.get("availableMoves"))

        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")

    def test_pawn_move_validation(self):

        r = requests.get(BASE + "/pawn/h2/h3")
        status = 200
        fig = "pawn"
        field = "h2"
        data = r.json().get("data")
        error = None
        move = "valid"

        assert status == r.status_code

        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")
        assert move == data.get("move")

        r = requests.get(BASE + "/pawn/h2/h8")
        status = 200
        fig = "pawn"
        field = "h2"
        data = r.json().get("data")
        error = "Current move is not permitted."
        move = "invalid"

        assert status == r.status_code

        assert fig == data.get("figure")
        assert field == data.get("currentField")
        assert error == data.get("error")
        assert move == data.get("move")

    def test_errors(self):
        r = requests.get(BASE + "/paasdasd/")
        assert r.status_code == 404

        r = requests.get(BASE + "/paasdasd/64")
        status = 409
        data = r.json().get("data")
        error = "Field does not exist."
        assert status == r.status_code

        assert error == data.get("error")

        r = requests.get(BASE + "/rook/64")
        status = 409
        data = r.json().get("data")
        error = "Field does not exist."
        assert status == r.status_code

        assert error == data.get("error")

        r = requests.get(BASE + "/rooks/b4")
        status = 404
        data = r.json().get("data")
        error = "Figure not found"
        assert status == r.status_code

        assert error == data.get("error")
