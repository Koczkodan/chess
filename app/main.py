from flask import Flask
from abc import ABCMeta, abstractmethod
from flask_restful import Resource, Api
import redis

app = Flask(__name__)
api = Api(app)
cache = redis.Redis(host="redis", port=6379)

board = [[chr(ord("a") + j) + str(8 - i) for j in range(8)] for i in range(8)]


class Chessman(object):
    __metaclass__ = ABCMeta

    def __init__(self, position):
        self.position = position
        x, y = self.position
        self.x, self.y = ord(x) - 97, 8 - int(y)
        super().__init__()

    @abstractmethod
    def list_available_moves(self):
        pass

    def validate_move(self, dest_field):
        return dest_field in self.list_available_moves()


class King(Chessman):
    def list_available_moves(self):
        _list = []
        x, y = self.x, self.y
        for i in range(-1, 2):
            for j in range(-1, 2):
                if (
                    y + i >= 0
                    and y + i < 8
                    and x + j >= 0
                    and x + j < 8
                    and (j != 0 or i != 0)
                ):
                    print(x + j, y + i)
                    _list.append(board[y + i][x + j])
        return _list


class Queen(Chessman):
    def list_available_moves(self):
        _list = []
        x, y = self.x, self.y
        _list += [i for i in board[y] if i != self.position]
        for i in range(8):
            if board[i][x] != self.position:
                _list.append(board[i][x])

        self.go(_list, x, y, 1, -1)
        self.go(_list, x, y, -1, 1)
        self.go(_list, x, y, -1, -1)
        self.go(_list, x, y, 1, 1)
        return _list

    def go(self, _list, x, y, a, b):
        if x + a >= 0 and x + a < 8 and y + b >= 0 and y + b < 8:
            self.go(_list, x + a, y + b, a, b)
            _list.append(board[y + b][x + a])


class Rook(Chessman):
    def list_available_moves(self):
        _list = []
        x, y = self.x, self.y
        _list += [i for i in board[y] if i != self.position] + [
            board[i][x] for i in range(8) if board[i][x] != self.position
        ]
        return _list


class Bishop(Chessman):
    def list_available_moves(self):
        _list = []
        x, y = self.x, self.y
        self.go(_list, x, y, 1, -1)
        self.go(_list, x, y, -1, 1)
        self.go(_list, x, y, -1, -1)
        self.go(_list, x, y, 1, 1)
        return _list

    def go(self, _list, x, y, a, b):
        if x + a >= 0 and x + a < 8 and y + b >= 0 and y + b < 8:
            self.go(_list, x + a, y + b, a, b)
            _list.append(board[y + b][x + a])


class Knight(Chessman):
    def list_available_moves(self):
        _list = []
        x, y = self.x, self.y
        mask = [
            [0, 1, 0, 1, 0],
            [1, 0, 0, 0, 1],
            [0, 0, 0, 0, 0],
            [1, 0, 0, 0, 1],
            [0, 1, 0, 1, 0],
        ]
        center = int(len(mask) / 2)
        for j in range(-2, 3):
            for i in range(-2, 3):
                if (
                    x + i >= 0
                    and x + i < 8
                    and y + j >= 0
                    and y + j < 8
                    and mask[center + j][center + i] == 1
                ):
                    _list.append(board[y + j][x + i])

        return _list


class Pawn(Chessman):
    def list_available_moves(self):
        _list = []
        x, y = self.x, self.y
        if y - 1 >= 0:
            _list.append(board[y - 1][x])
        return _list


class API(Resource):
    def get(self, figure, currentField):
        if currentField not in [item for i in board for item in i]:
            return {
                "data": {
                    "availableMoves": [],
                    "error": "Field does not exist.",
                    "figure": figure,
                    "currentField": currentField,
                }
            }, 409
        if King.__name__.upper() == figure.upper():
            king = King(currentField)
            return {
                "data": {
                    "availableMoves": king.list_available_moves(),
                    "error": None,
                    "figure": figure,
                    "currentField": currentField,
                }
            }
        if Queen.__name__.upper() == figure.upper():
            queen = Queen(currentField)
            return {
                "data": {
                    "availableMoves": queen.list_available_moves(),
                    "error": None,
                    "figure": figure,
                    "currentField": currentField,
                }
            }
        if Rook.__name__.upper() == figure.upper():
            rook = Rook(currentField)
            return {
                "data": {
                    "availableMoves": rook.list_available_moves(),
                    "error": None,
                    "figure": figure,
                    "currentField": currentField,
                }
            }
        if Bishop.__name__.upper() == figure.upper():
            bishop = Bishop(currentField)
            return {
                "data": {
                    "availableMoves": bishop.list_available_moves(),
                    "error": None,
                    "figure": figure,
                    "currentField": currentField,
                }
            }
        if Knight.__name__.upper() == figure.upper():
            knight = Knight(currentField)
            return {
                "data": {
                    "availableMoves": knight.list_available_moves(),
                    "error": None,
                    "figure": figure,
                    "currentField": currentField,
                }
            }
        if Pawn.__name__.upper() == figure.upper():
            pawn = Pawn(currentField)
            return {
                "data": {
                    "availableMoves": pawn.list_available_moves(),
                    "error": None,
                    "figure": figure,
                    "currentField": currentField,
                }
            }

        return {
            "data": {
                "availableMoves": [],
                "error": "Figure not found",
                "figure": figure,
                "currentField": currentField,
            }
        }, 404


class APIDest(Resource):
    def get(self, figure, currentField, dF):
        if currentField not in [item for i in board for item in i]:
            return {
                "data": {
                    "move": "invalid",
                    "figure": figure,
                    "error": "Field does not exist.",
                    "currentField": currentField,
                    "destField": dF,
                }
            }, 409

        if dF not in [item for i in board for item in i]:
            return {
                "data": {
                    "move": "invalid",
                    "figure": figure,
                    "error": "Destination field is invalid",
                    "currentField": currentField,
                    "destField": dF,
                }
            }, 409

        if King.__name__.upper() == figure.upper():
            king = King(currentField)
            return {
                "data": {
                    "move": "valid" if king.validate_move(dF) else "invalid",
                    "figure": figure,
                    "error": None
                    if king.validate_move(dF)
                    else "Current move is not permitted.",
                    "currentField": currentField,
                    "destField": dF,
                }
            }
        if Queen.__name__.upper() == figure.upper():
            queen = Queen(currentField)
            return {
                "data": {
                    "move": "valid" if queen.validate_move(dF) else "invalid",
                    "figure": figure,
                    "error": None
                    if queen.validate_move(dF)
                    else "Current move is not permitted.",
                    "currentField": currentField,
                    "destField": dF,
                }
            }
        if Rook.__name__.upper() == figure.upper():
            rook = Rook(currentField)
            return {
                "data": {
                    "move": "valid" if rook.validate_move(dF) else "invalid",
                    "figure": figure,
                    "error": None
                    if rook.validate_move(dF)
                    else "Current move is not permitted.",
                    "currentField": currentField,
                    "destField": dF,
                }
            }
        if Bishop.__name__.upper() == figure.upper():
            bishop = Bishop(currentField)
            return {
                "data": {
                    "move": "valid" if bishop.validate_move(dF) else "invalid",
                    "figure": figure,
                    "error": None
                    if bishop.validate_move(dF)
                    else "Current move is not permitted.",
                    "currentField": currentField,
                    "destField": dF,
                }
            }
        if Knight.__name__.upper() == figure.upper():
            knight = Knight(currentField)
            return {
                "data": {
                    "move": "valid" if knight.validate_move(dF) else "invalid",
                    "figure": figure,
                    "error": None
                    if knight.validate_move(dF)
                    else "Current move is not permitted.",
                    "currentField": currentField,
                    "destField": dF,
                }
            }
        if Pawn.__name__.upper() == figure.upper():
            pawn = Pawn(currentField)
            return {
                "data": {
                    "move": "valid" if pawn.validate_move(dF) else "invalid",
                    "figure": figure,
                    "error": None
                    if pawn.validate_move(dF)
                    else "Current move is not permitted.",
                    "currentField": currentField,
                    "destField": dF,
                }
            }

        return {
            "data": {
                "move": "invalid",
                "figure": figure,
                "error": "Figure not found.",
                "currentField": currentField,
                "destField": dF,
            }
        }, 404


api_dest_str = "/api/v1/<string:figure>/<string:currentField>/<string:dF>"
api_get_str = "/api/v1/<string:figure>/<string:currentField>"
api.add_resource(API, api_get_str)
api.add_resource(APIDest, api_dest_str)

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
